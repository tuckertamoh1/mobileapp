package cit.fms.mobile_app.cineverse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by thomasohalloran on 23/04/2017.
 */

public class MyAdapter extends ArrayAdapter{

    private ScreenSchedule sched;
    private Movie mov;

    public MyAdapter(Context context, ArrayList<ScreenSchedule> s) {
        super(context, 0, s);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        sched = (ScreenSchedule) getItem(position);
        mov = sched.getMovie();

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
        }

        //Fill row with all attributes
        ImageView imgView = (ImageView) convertView.findViewById(R.id.filmImg);
        if(!sched.getMovie().getBase64Img().equalsIgnoreCase("")) {
            byte [] imageAsBytes = Base64.decode(sched.getMovie().getBase64Img(), Base64.DEFAULT);
            imgView.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        }
        else {
            imgView.setImageResource(R.mipmap.ic_launcher);
        }




        //String aaa = mov.getName();
        TextView filmTitle = (TextView) convertView.findViewById(R.id.filmTitle);
        filmTitle.setPaintFlags(filmTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        filmTitle.setText(sched.getMovie().getName());

        TextView filmDirector = (TextView) convertView.findViewById(R.id.filmDirector);
        filmDirector.setText("Director: "+sched.getMovie().getDirector());

        TextView filmGenre = (TextView) convertView.findViewById(R.id.filmGenre);
        filmGenre.setText("Genre: "+sched.getMovie().getGenre());

        TextView filmRunTime = (TextView) convertView.findViewById(R.id.filmRunTime);
        filmRunTime.setText("Runtime: "+sched.getMovie().getDuration()+" minutes");

        TextView filmAgeRating = (TextView) convertView.findViewById(R.id.filmAgeRating);
        filmAgeRating.setText("Age Rating: "+sched.getMovie().getAgeRating());

        TextView filmDescription = (TextView) convertView.findViewById(R.id.filmDescription);
        filmDescription.setText(sched.getMovie().getDescription());

        //Button theBtn = (Button) convertView.findViewById(R.id.btn1);
        //theBtn.setOnClickListener(new View.OnClickListener() {
           // @Override
            //public void onClick(View v) {
              //  Toast.makeText(getContext(), "gdsgdsgs", Toast.LENGTH_LONG).show();
            //}
        //});

        // Lookup view for data population
        //TextView tvName = (TextView) convertView.findViewById(R.id.view1);
        // Populate the data into the template view using the data object
        //tvName.setText("Screen"+s.getScreenNum());
        // Return the completed view to render on screen
        return convertView;
    }
}
