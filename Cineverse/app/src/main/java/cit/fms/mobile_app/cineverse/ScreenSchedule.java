package cit.fms.mobile_app.cineverse;

import java.util.ArrayList;

/**
 * Created by thomasohalloran on 21/04/2017.
 */

public class ScreenSchedule {

    private int screenID;
    private int screenNum;
    private int capacity;
    private int ticketsSold;

    private Movie movie;
    private ArrayList<String> times = new ArrayList<>();

    public ScreenSchedule () {}

    public ScreenSchedule(int screenID, int screenNum, int capacity, int ticketsSold, Movie movie, ArrayList<String> times) {
        this.screenID = screenID;
        this.screenNum = screenNum;
        this.capacity = capacity;
        this.ticketsSold = ticketsSold;
        this.movie = movie;
        this.times = times;
    }

    public int getScreenID() {
        return screenID;
    }

    public void setScreenID(int screenID) {
        this.screenID = screenID;
    }

    public int getScreenNum() {
        return screenNum;
    }

    public void setScreenNum(int screenNum) {
        this.screenNum = screenNum;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getTicketsSold() {
        return ticketsSold;
    }

    public void setTicketsSold(int ticketsSold) {
        this.ticketsSold = ticketsSold;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public ArrayList<String> getTimes() {
        return times;
    }

    public void setTimes(ArrayList<String> times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return "ScreenSchedule{" +
                "screenNum=" + screenNum +
                ", capacity=" + capacity +
                ", movie=" + movie +
                '}';
    }
}
