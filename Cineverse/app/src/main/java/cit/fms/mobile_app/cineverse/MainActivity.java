package cit.fms.mobile_app.cineverse;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

public class MainActivity extends AppCompatActivity {

    //static String serverUrl = "http://192.168.1.11:3458"; //Home
    static String serverUrl = "http://192.168.43.235:3458"; //Mobile
    static String location;
    boolean locationSelected = false;
    static ArrayList<ScreenSchedule> displayList;
    static ArrayList<Location> locations;
    static ArrayList<String> locNames;
    static ArrayList<Price> prices;
    static ScreenSchedule tempSched;
    static TextView testData;
    static Location tempLocation;
    static int numAdults, numOap, numStudent, numChild;
    static String show_time;
    static double totalCost;
    Spinner locationSpinner;
    Button loadLocationBtn;
    Intent intent;
    static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Downloading data, please wait...");
        progressDialog.show();

        //Use for spinner
        Location dummy = new Location();
        dummy.setName("Select a location:");

        tempLocation = new Location();



        testData = (TextView) findViewById(R.id.testData);
        loadLocationBtn = (Button) findViewById(R.id.btnLocationLoad);

        displayList = new ArrayList<ScreenSchedule>();
        locations = new ArrayList<Location>();
        locNames = new ArrayList<String>();
        locNames.add("Select a location:");
        prices = new ArrayList<Price>();


        new JSONTask("getLocations", getApplicationContext()).execute(serverUrl+"/getAllLocationsActive");


        locationSpinner = (Spinner) findViewById(R.id.locSpinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, locNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.notifyDataSetChanged();

        locationSpinner.setAdapter(adapter);

        locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if( position > 0) {
                    locationSelected = true;
                    tempLocation = locations.get(position-1);
                }
                else {
                    locationSelected = false;
                };
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        loadLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(locationSelected) {
                    //Toast.makeText(MainActivity.this, tempLocation.getName(), Toast.LENGTH_LONG).show();
                    new JSONTask("getLocationMovieSchedules", getApplicationContext(), tempLocation).execute(serverUrl+"/getLocationMovieSchedules");
                    intent = new Intent(MainActivity.this, DisplayActivity.class);

                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(MainActivity.this, "No location selected!!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Quit?");
        dialog.setMessage("Do you really want to quit?");

        dialog.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }
}
