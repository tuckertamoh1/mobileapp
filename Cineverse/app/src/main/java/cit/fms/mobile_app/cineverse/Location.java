package cit.fms.mobile_app.cineverse;

/**
 * Created by thomasohalloran on 20/04/2017.
 */

public class Location {

    private int id;
    private String name;
    private String addr;
    private int numberOfScreens;
    private String locationStatus;
    private int f_id;

    public Location() {
    }

    public Location(int id, String name, String addr, int numberOfScreens, String locationStatus, int f_id) {

        this.id = id;
        this.name = name;
        this.addr = addr;
        this.numberOfScreens = numberOfScreens;
        this.locationStatus = locationStatus;
        this.f_id = f_id;
    }


    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public int getNumberOfScreens() {
        return numberOfScreens;
    }

    public void setNumberOfScreens(int numberOfScreens) {
        this.numberOfScreens = numberOfScreens;
    }

    public String getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(String locationStatus) {
        this.locationStatus = locationStatus;
    }

    public int getF_id() {
        return f_id;
    }

    public void setF_id(int f_id) {
        this.f_id = f_id;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
