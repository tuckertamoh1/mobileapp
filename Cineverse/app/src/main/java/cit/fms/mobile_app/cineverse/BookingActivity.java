package cit.fms.mobile_app.cineverse;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static cit.fms.mobile_app.cineverse.MainActivity.*;

/**
 * Created by thomasohalloran on 19/04/2017.
 */

public class BookingActivity extends AppCompatActivity {

    TextView bookingTitle, adultTextView, studentTextView, oapTextView, childTextView, currTotal;
    ImageButton backBtn;
    Button cancelTicketBtn, confirmTicketBtn;
    Spinner timeSpinner, adultSpinner, studentSpinner, childSpinner, oapSpinner;
    DatePicker cal;
    ArrayList<String> times;
    ArrayList<String> tickNumList;
    private String selectedTime;
    Price price;
    NumberFormat nf;
    boolean pricesSelected;
    private int year;
    private int month;
    private int day;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        pricesSelected = false;
        nf = new DecimalFormat("##.00");
        show_time = "";
        numAdults = 0; numOap = 0; numStudent = 0; numChild = 0;
        totalCost = 0.00;


        tickNumList = new ArrayList<String>(Arrays.asList("0","1","2","3","4","5","6","7","8","9","10"));

        times = MainActivity.tempSched.getTimes();
        if( !times.get(0).equalsIgnoreCase("Select a show time:")) {
            times.add(0, "Select a show time:");
        }

        bookingTitle = (TextView) findViewById(R.id.bookingTitle);
        backBtn = (ImageButton) findViewById(R.id.backBtn);
        timeSpinner = (Spinner) findViewById(R.id.timeSpinner);
        adultTextView = (TextView) findViewById(R.id.adultPrice);
        adultSpinner = (Spinner) findViewById(R.id.adultSpinner);
        childTextView = (TextView) findViewById(R.id.childPrice);
        childSpinner = (Spinner) findViewById(R.id.childSpinner);
        studentTextView = (TextView) findViewById(R.id.studentPrice);
        studentSpinner = (Spinner) findViewById(R.id.studentSpinner);
        oapTextView = (TextView) findViewById(R.id.oapPrice);
        oapSpinner = (Spinner) findViewById(R.id.oapSpinner);
        currTotal = (TextView) findViewById(R.id.currTotal);
        cancelTicketBtn = (Button) findViewById(R.id.cancelTicketBtn);
        confirmTicketBtn = (Button) findViewById(R.id.confirmTicketBtn);


        bookingTitle.setText("Selected Movie: \n"+MainActivity.tempSched.getMovie().getName());

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("getLocationMovieSchedules", getApplicationContext(), tempLocation).execute(serverUrl+"/getLocationMovieSchedules");
                Intent intent = new Intent(getApplicationContext(), DisplayActivity.class);
                startActivity(intent);

                finish();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, times);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();

        timeSpinner.setAdapter(adapter);
        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if( position > 0) {
                    pricesSelected = true;
                    selectedTime = times.get(position).substring(0, 2);
                    show_time = times.get(position);
                    double temp = Double.parseDouble(selectedTime);
                    if( temp >= 18) {
                        for(int i = 0; i < prices.size();i++) {
                            if(prices.get(i).getPriceType().equalsIgnoreCase("evening")) {
                                price = prices.get(i);
                                adultTextView.setText(nf.format(price.getAdult()));
                                adultSpinner.setSelection(0);
                                studentTextView.setText(nf.format(price.getStudent()));
                                studentSpinner.setSelection(0);
                                oapTextView.setText(nf.format(price.getOap()));
                                oapSpinner.setSelection(0);
                                childTextView.setText(nf.format(price.getChild()));
                                childSpinner.setSelection(0);
                                numAdults = 0; numOap = 0; numStudent = 0; numChild = 0; totalCost = 0;
                                currTotal.setText("Current Total: €"+nf.format(totalCost));
                                break;
                            }
                        }
                    }
                    if( temp < 18) {
                        for(int i = 0; i < prices.size();i++) {
                            if(prices.get(i).getPriceType().equalsIgnoreCase("afternoon")) {
                                price = prices.get(i);
                                adultTextView.setText(nf.format(price.getAdult()));
                                adultSpinner.setSelection(0);
                                studentTextView.setText(nf.format(price.getStudent()));
                                studentSpinner.setSelection(0);
                                oapTextView.setText(nf.format(price.getOap()));
                                oapSpinner.setSelection(0);
                                childTextView.setText(nf.format(price.getChild()));
                                childSpinner.setSelection(0);
                                numAdults = 0; numOap = 0; numStudent = 0; numChild = 0; totalCost = 0;
                                currTotal.setText("Current Total: €"+nf.format(totalCost));
                                break;
                            }
                        }
                    }
                }
                else {
                    adultTextView.setText("");
                    adultSpinner.setSelection(0);
                    studentTextView.setText("");
                    studentSpinner.setSelection(0);
                    oapTextView.setText("");
                    oapSpinner.setSelection(0);
                    childTextView.setText("");
                    childSpinner.setSelection(0);
                    numAdults = 0; numOap = 0; numStudent = 0; numChild = 0; totalCost = 0;
                    currTotal.setText("Current Total: €"+nf.format(totalCost));
                    pricesSelected = false;
                };
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<String> adapterNums = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tickNumList);
        adapterNums.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterNums.notifyDataSetChanged();

        adultSpinner.setAdapter(adapterNums);
        adultSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (pricesSelected) {
                    int temp = position;
                    if ( temp > 0 && numAdults == 0 ) {
                        totalCost += temp*price.getAdult();
                        numAdults = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numAdults > 0 && temp != 0 ) {
                        totalCost -= numAdults*price.getAdult();
                        totalCost += temp*price.getAdult();
                        numAdults = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numAdults > 0 && temp == 0) {
                        totalCost -= numAdults*price.getAdult();
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    };
                }
                else {
                    adultSpinner.setSelection(0);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        studentSpinner.setAdapter(adapterNums);
        studentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (pricesSelected) {
                    int temp = position;
                    if ( temp > 0 && numStudent == 0 ) {
                        totalCost += temp*price.getStudent();
                        numStudent = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numStudent > 0 && temp != 0 ) {
                        totalCost -= numStudent*price.getAdult();
                        totalCost += temp*price.getStudent();
                        numStudent = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numStudent > 0 && temp == 0) {
                        totalCost -= numStudent*price.getStudent();
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    };
                }
                else {
                    studentSpinner.setSelection(0);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        oapSpinner.setAdapter(adapterNums);
        oapSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (pricesSelected) {
                    int temp = position;
                    if ( temp > 0 && numOap == 0 ) {
                        totalCost += temp*price.getOap();
                        numOap = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numOap > 0 && temp != 0 ) {
                        totalCost -= numOap*price.getOap();
                        totalCost += temp*price.getOap();
                        numOap = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numOap > 0 && temp == 0) {
                        totalCost -= numOap*price.getOap();
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    };
                }
                else {
                    oapSpinner.setSelection(0);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        childSpinner.setAdapter(adapterNums);
        childSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (pricesSelected) {
                    int temp = position;
                    if ( temp > 0 && numChild == 0 ) {
                        totalCost += temp*price.getChild();
                        numChild = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numChild > 0 && temp != 0 ) {
                        totalCost -= numChild*price.getChild();
                        totalCost += temp*price.getChild();
                        numChild = temp;
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    }
                    else if ( numChild > 0 && temp == 0) {
                        totalCost -= numChild*price.getChild();
                        currTotal.setText("Current Total: €"+nf.format(totalCost));
                    };
                }
                else {
                    childSpinner.setSelection(0);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        cancelTicketBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("getLocationMovieSchedules", getApplicationContext(), tempLocation).execute(serverUrl+"/getLocationMovieSchedules");
                Intent intent = new Intent(getApplicationContext(), DisplayActivity.class);
                startActivity(intent);
                finish();
            }
        });

        confirmTicketBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check all tickets and caluclate total cost
                Toast.makeText(BookingActivity.this, "€"+nf.format(totalCost), Toast.LENGTH_LONG).show();

                new JSONTask("makeBooking", getApplicationContext(), tempLocation).execute(MainActivity.serverUrl+"/bookTickets");

                //Do this only if a valid ticket total
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Quit?");
        dialog.setMessage("Do you really want to quit?");

        dialog.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BookingActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }
}
