package cit.fms.mobile_app.cineverse;

/**
 * Created by thomasohalloran on 24/04/2017.
 */

public class Price {

    private String priceType;
    private double adult, student, oap, child;
    private int location;

    public Price () {};

    public Price(String priceType, double adult, double student, double oap, double child, int location) {
        this.priceType = priceType;
        this.adult = adult;
        this.student = student;
        this.oap = oap;
        this.child = child;
        this.location = location;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public double getAdult() {
        return adult;
    }

    public void setAdult(double adult) {
        this.adult = adult;
    }

    public double getStudent() {
        return student;
    }

    public void setStudent(double student) {
        this.student = student;
    }

    public double getOap() {
        return oap;
    }

    public void setOap(double oap) {
        this.oap = oap;
    }

    public double getChild() {
        return child;
    }

    public void setChild(double child) {
        this.child = child;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Price{" +
                "priveType='" + priceType + '\'' +
                ", adult='" + adult + '\'' +
                ", student='" + student + '\'' +
                ", oap='" + oap + '\'' +
                ", child='" + child + '\'' +
                ", location=" + location +
                '}';
    }
}
