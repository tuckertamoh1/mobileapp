package cit.fms.mobile_app.cineverse;

/**
 * Created by thomasohalloran on 21/04/2017.
 */

public class Movie {

    private int id;
    private String name, director;
    private String description;
    private String base64Img;
    private String ageRating;
    private int duration;
    private String genre;

    public Movie () {};

    public Movie(int id, String name, String director, String description, String base64Img, String ageRating, int duration, String genre) {
        this.id = id;
        this.name = name;
        this.director = director;
        this.description = description;
        this.base64Img = base64Img;
        this.ageRating = ageRating;
        this.duration = duration;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDirector() {
        return director;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBase64Img() {
        return base64Img;
    }

    public void setBase64Img(String base64Img) {
        this.base64Img = base64Img;
    }

    public String getAgeRating() {
        return ageRating;
    }

    public void setAgeRating(String ageRating) {
        this.ageRating = ageRating;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return name;
    }
}
