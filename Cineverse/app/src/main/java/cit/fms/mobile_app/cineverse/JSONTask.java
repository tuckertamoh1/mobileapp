package cit.fms.mobile_app.cineverse;

/**
 * Created by thomasohalloran on 20/04/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import static cit.fms.mobile_app.cineverse.MainActivity.*;

public class JSONTask extends AsyncTask<String, String, String>{

    String error = "";
    private String actionType;
    private Location l;
    private String jsonText;
    int postGet = 1;
    Context context;
    HttpURLConnection urlConnection = null;
    URL url = null;
    DataOutputStream dos = null;
    BufferedReader in = null;
    StringBuffer response = null;
    JSONObject jsonObj;


    public JSONTask(String action, Context con)
    {

        actionType = action;
        context = con;
    }

    //Used to send location object to async task
    public JSONTask(String action, Context con, Location l)
    {
        this.l = l;
        actionType = action;
        context = con;
    }

    protected String doInBackground(String... params)
    {

        try {

            jsonObj = new JSONObject();
            jsonText = "";

            url = new URL(params[0]);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true); // Set Http method to POST
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            dos = new DataOutputStream(urlConnection.getOutputStream());


            if(actionType == "getLocations") {
                jsonObj.put("id", "1");
                jsonObj.put("status","active");
                jsonText = jsonObj.toString();
            }

            if(actionType == "getPrices") {
                jsonObj.put("f_id", "1");
                jsonObj.put("l_id", l.getID()+"");
                jsonText = jsonObj.toString();
            }

            if(actionType == "getLocationMovieSchedules") {
                jsonObj.put("f_id", "1");
                jsonObj.put("l_id", l.getID()+"");
                jsonText = jsonObj.toString();
            }

            if(actionType == "makeBooking") {
                jsonObj.put("f_id", "1");
                jsonObj.put("l_id", l.getID()+"");
                jsonObj.put("s_id", tempSched.getScreenID()+"");
                jsonObj.put("m_id", tempSched.getMovie().getId()+"");
                jsonObj.put("show_time", show_time);
                jsonObj.put("num_adults", numAdults+"");
                jsonObj.put("num_child", numChild+"");
                jsonObj.put("num_oap", numOap+"");
                jsonObj.put("num_student", numStudent+"");
                jsonObj.put("total_cost", totalCost+"");
                jsonText = jsonObj.toString();
            }

            dos.writeBytes(jsonText);
            dos.flush();
            dos.close();

            String inputLine;
            in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        }
        catch (Exception e){};
        return null;
    }

    @Override
    protected void onPostExecute(String result)
    {

        JSONArray json = null;
        JSONObject jsonObj = null;
        Movie m = null;
        ScreenSchedule sched = null;
        ArrayList<String> tempTimes = null;

        try {

            if(actionType == "getLocations") {
                json = new JSONArray(result);
                for( int i = 0; i < json.length();i++) {
                    jsonObj = new JSONObject(json.get(i).toString());
                    Location l = new Location(
                            Integer.parseInt(jsonObj.getString("id")),
                            jsonObj.getString("name"),
                            jsonObj.getString("address"),
                            Integer.parseInt(jsonObj.getString("num_screens")),
                            jsonObj.getString("l_status"),
                            Integer.parseInt(jsonObj.getString("f_id"))
                    );
                    locations.add(l);
                    locNames.add(jsonObj.getString("name"));
                }
                MainActivity.progressDialog.dismiss();
            }

            if(actionType == "getPrices") {
                json = new JSONArray(result);
                for( int i = 0; i < json.length();i++) {
                    jsonObj = new JSONObject(json.get(i).toString());
                    Price p = new Price(
                            jsonObj.getString("priceType"),
                            Double.parseDouble(jsonObj.getString("adult")),
                            Double.parseDouble(jsonObj.getString("student")),
                            Double.parseDouble(jsonObj.getString("oap")),
                            Double.parseDouble(jsonObj.getString("child")),
                            Integer.parseInt(jsonObj.getString("l_id"))
                    );
                    prices.add(p);
                }
            }

            if(actionType == "getLocationMovieSchedules") {

                //Toast.makeText(this.context, result, Toast.LENGTH_LONG).show();
                //DisplayActivity.testData1.setText(result);



                json = new JSONArray(result);
                int numScreens = l.getNumberOfScreens();
                boolean found;
                for(int i = 1; i<numScreens;i++) {
                    m = null;
                    sched = new ScreenSchedule();
                    tempTimes = new ArrayList<>();
                    found = false;

                    for( int j = 0; j < json.length();j++) {
                        jsonObj = new JSONObject(json.get(j).toString());
                        int temp = Integer.parseInt(jsonObj.getString("number"));
                        if( Integer.parseInt(jsonObj.getString("number")) == i) {

                            if( m == null ) {
                                found = true;
                                sched.setCapacity(Integer.parseInt(jsonObj.getString("capacity")));
                                sched.setTicketsSold(Integer.parseInt(jsonObj.getString("tickets_booked")));
                                sched.setScreenID(Integer.parseInt(jsonObj.getString("s_id")));
                                m = new Movie(
                                        Integer.parseInt(jsonObj.getString("m_id")),
                                        jsonObj.getString("title"),
                                        jsonObj.getString("director"),
                                        jsonObj.getString("description"),
                                        jsonObj.getString("movie_img"),
                                        jsonObj.getString("age_rating"),
                                        Integer.parseInt(jsonObj.getString("duration")),
                                        jsonObj.getString("genre")
                                );
                                sched.setMovie(m);
                            };
                            tempTimes.add(jsonObj.getString("movie_time"));
                        };
                    };

                    if (found) {

                        sched.setScreenNum(i);
                        sched.setMovie(m);
                        sched.setTimes(tempTimes);
                        MainActivity.displayList.add(sched);
                    };
                };
                //DisplayActivity.adapter = new MyAdapter(this, displayList);
                //DisplayActivity.listview.setText(result);
                DisplayActivity.adapter.notifyDataSetChanged();
                DisplayActivity.progressDisplayDialog.dismiss();
            }

            if( actionType == "makeBooking") {
                Toast.makeText(this.context, result, Toast.LENGTH_LONG).show();
            }
        }
        catch( Exception e ) {};

    }
}
