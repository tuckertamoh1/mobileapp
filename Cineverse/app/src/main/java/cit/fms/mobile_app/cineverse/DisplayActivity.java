package cit.fms.mobile_app.cineverse;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;


/**
 * Created by thomasohalloran on 19/04/2017.
 */

public class DisplayActivity extends AppCompatActivity {

    ListView displayView;
    ImageButton homeBtn;
    //static ArrayList<ScreenSchedule> displayList;
    static MyAdapter adapter;
    static ProgressDialog progressDisplayDialog;
    static boolean loaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        progressDisplayDialog = new ProgressDialog(DisplayActivity.this);
        progressDisplayDialog.setMessage("Downloading movies, please wait...");
        progressDisplayDialog.show();

        new JSONTask("getPrices", getApplicationContext(), MainActivity.tempLocation).execute(MainActivity.serverUrl+"/getAllPricesForLocation");

        displayView = (ListView) findViewById(R.id.displayListView);
        homeBtn = (ImageButton) findViewById(R.id.homeBtn);

        MainActivity.displayList = new ArrayList<ScreenSchedule>();
        adapter = new MyAdapter(this, MainActivity.displayList);

        displayView.setAdapter(adapter);

        displayView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //ScreenSchedule sched = (ScreenSchedule) parent.getItemAtPosition(position);
                MainActivity.tempSched = (ScreenSchedule) parent.getItemAtPosition(position);
                //Toast.makeText(DisplayActivity.this, MainActivity.tempSched.toString(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), BookingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });



    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Quit?");
        dialog.setMessage("Do you really want to quit?");

        dialog.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DisplayActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }
}
